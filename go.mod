module bitbucket.org/_metalogic_/boggle

go 1.12

require (
	bitbucket.org/_metalogic_/log v1.3.0
	github.com/gdamore/tcell v1.1.2
	github.com/rivo/tview v0.0.0-20190721135419-23dc8a0944e4
	golang.org/x/sys v0.0.0-20190602015325-4c4f7f33c9ed // indirect
)
