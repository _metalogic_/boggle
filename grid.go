package boggle

import (
	"fmt"

	"bitbucket.org/_metalogic_/log"
)

type cell struct {
	letter    byte
	neighbors []*cell
}

func (c *cell) String() string {
	return string(c.letter)
}

// grid is the 4x4 grid of letter cells
type grid [4][4]*cell

// newGrid creates an empty grid that will hold letters of a game
func newGrid() (grid grid) {

	// generate a 4x4 grid of empty cells
	for row := 0; row < 4; row++ {
		for col := 0; col < 4; col++ {
			grid[row][col] = &cell{}
		}
	}
	// populate the adjacency array for each cell in the grid
	for row := 0; row < 4; row++ {
		for col := 0; col < 4; col++ {
			if row-1 >= 0 {
				grid[row][col].neighbors = append(grid[row][col].neighbors, grid[row-1][col])
				if col-1 >= 0 {
					grid[row][col].neighbors = append(grid[row][col].neighbors, grid[row-1][col-1])
				}
				if col+1 < 4 {
					grid[row][col].neighbors = append(grid[row][col].neighbors, grid[row-1][col+1])
				}
			}
			if row+1 < 4 {
				grid[row][col].neighbors = append(grid[row][col].neighbors, grid[row+1][col])
				if col-1 >= 0 {
					grid[row][col].neighbors = append(grid[row][col].neighbors, grid[row+1][col-1])
				}
				if col+1 < 4 {
					grid[row][col].neighbors = append(grid[row][col].neighbors, grid[row+1][col+1])
				}
			}
			if col-1 >= 0 {
				grid[row][col].neighbors = append(grid[row][col].neighbors, grid[row][col-1])
			}
			if col+1 < 4 {
				grid[row][col].neighbors = append(grid[row][col].neighbors, grid[row][col+1])
			}

		}
	}

	grid.roll()

	if log.Loggable(log.DebugLevel) {
		printAdjacencies(grid)
	}

	return grid
}

// checkPath returns true if the byte sequence is a valid path starting on cell c in the grid
func checkPath(bytes []byte, c *cell, visited []*cell) bool {

	if len(bytes) == 0 {
		return true
	}

	if bytes[0] != c.letter {
		return false
	}

	// a cell may only be used once in a path
	for _, v := range visited {
		if c == v {
			return false
		}
	}

	visited = append(visited, c)

	log.Debugf("checking %s\n", string(bytes))

	for _, neighbor := range c.neighbors {
		if checkPath(bytes[1:], neighbor, visited) {
			return true
		}
	}
	return false
}

// roll populates grid with random letters from a roll of the dice
func (grid grid) roll() {
	for row := 0; row < 4; row++ {
		for col := 0; col < 4; col++ {
			grid[row][col].letter = roll(row + col)
		}
	}
}

// cell returns the string for grid cell row, col
func (grid grid) cell(row, col int) string {
	if grid[row][col].letter == 'Q' {
		return "  Qu "
	}
	return fmt.Sprintf("  %c  ", grid[row][col].letter)
}

// Print prints a simple text representation of the grid
func (grid grid) Print() {
	for row := 0; row < 4; row++ {
		fmt.Println("\n  ---   ---   ---   ---")
		fmt.Print("|")
		for col := 0; col < 4; col++ {
			if grid[row][col].letter == 'Q' {
				fmt.Printf("  Qu |")
				continue
			}
			fmt.Printf("  %c  |", grid[row][col].letter)
		}
	}
	fmt.Println("\n  ---   ---   ---   ---")
}

// printAdjacencies ...
func printAdjacencies(grid grid) {
	for row := 0; row < 4; row++ {
		for col := 0; col < 4; col++ {
			fmt.Printf("Grid[%d][%d]: %c => %v\n", row, col, grid[row][col].letter, grid[row][col].neighbors)
		}
	}
}
