package boggle

import (
	"reflect"
	"testing"
)

type t struct {
	word     string
	list     []string
	inserted []string
}

var tests = []*t{
	&t{"foozle", []string{}, []string{"foozle"}},
	&t{"foozle", []string{"bazzle"}, []string{"bazzle", "foozle"}},
	&t{"bazzle", []string{"foozle"}, []string{"bazzle", "foozle"}},
	&t{"foozle", []string{"bazzle", "dazzle", "razzle"}, []string{"bazzle", "dazzle", "foozle", "razzle"}},
	&t{"bazzle", []string{"bazzle", "dazzle", "foozle", "razzle"}, []string{"bazzle", "dazzle", "foozle", "razzle"}},
	&t{"foozle", []string{"bazzle", "dazzle", "foozle", "razzle"}, []string{"bazzle", "dazzle", "foozle", "razzle"}},
	&t{"razzle", []string{"bazzle", "dazzle", "foozle", "razzle"}, []string{"bazzle", "dazzle", "foozle", "razzle"}},
	&t{"zoozle", []string{"bazzle", "dazzle", "foozle", "razzle"}, []string{"bazzle", "dazzle", "foozle", "razzle", "zoozle"}},
	&t{"dazzle", []string{"bazzle", "dazzle", "foozle", "razzle"}, []string{"bazzle", "dazzle", "foozle", "razzle"}},
}

func TestSortedInsert(t *testing.T) {
	for _, test := range tests {
		result := sortedInsert(test.word, test.list)
		if !reflect.DeepEqual(test.inserted, result) {
			t.Errorf("expected %v, but got %s\n", test.inserted, result)
		}
	}
}
