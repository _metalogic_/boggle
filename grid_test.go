package boggle

import (
	"testing"
)

var (
	testGrid1 = newGrid()
	testGrid2 = newGrid()
)

/***
 Grid1
  ---   ---   ---   ---
|  Qu |  I  |  T  |  O  |
  ---   ---   ---   ---
|  A  |  C  |  K  |  O  |
  ---   ---   ---   ---
|  D  |  O  |  O  |  O  |
  ---   ---   ---   ---
|  R  |  A  |  N  |  T  |
  ---   ---   ---   ---
  ***/

/***
 Grid2 - quadricentennials - one of the Boggle 17 letter words
  ---   ---   ---   ---
|  Qu |  A  |  D  |  R  |
  ---   ---   ---   ---
|  N  |  E  |  C  |  I  |
  ---   ---   ---   ---
|  T  |  E  |  N  |  N  |
  ---   ---   ---   ---
|  S  |  L  |  A  |  I  |
  ---   ---   ---   ---
  ***/
func init() {
	testGrid1[0][0].letter = 'Q'
	testGrid1[0][1].letter = 'I'
	testGrid1[0][2].letter = 'T'
	testGrid1[0][3].letter = 'O'
	testGrid1[1][0].letter = 'A'
	testGrid1[1][1].letter = 'C'
	testGrid1[1][2].letter = 'K'
	testGrid1[1][3].letter = 'O'
	testGrid1[2][0].letter = 'D'
	testGrid1[2][1].letter = 'O'
	testGrid1[2][2].letter = 'O'
	testGrid1[2][3].letter = 'O'
	testGrid1[3][0].letter = 'R'
	testGrid1[3][1].letter = 'A'
	testGrid1[3][2].letter = 'N'
	testGrid1[3][3].letter = 'T'

	testGrid2[0][0].letter = 'Q'
	testGrid2[0][1].letter = 'A'
	testGrid2[0][2].letter = 'D'
	testGrid2[0][3].letter = 'R'
	testGrid2[1][0].letter = 'N'
	testGrid2[1][1].letter = 'E'
	testGrid2[1][2].letter = 'C'
	testGrid2[1][3].letter = 'I'
	testGrid2[2][0].letter = 'T'
	testGrid2[2][1].letter = 'E'
	testGrid2[2][2].letter = 'N'
	testGrid2[2][3].letter = 'N'
	testGrid2[3][0].letter = 'S'
	testGrid2[3][1].letter = 'L'
	testGrid2[3][2].letter = 'A'
	testGrid2[3][3].letter = 'I'

}

type check struct {
	word  []byte
	cell  *cell
	check bool
}

var pathTests = []*check{
	&check{[]byte("QIT"), testGrid1[0][0], true},
	&check{[]byte("QORA"), testGrid1[0][0], false},
	&check{[]byte("QACK"), testGrid1[0][0], true},
	&check{[]byte("QAD"), testGrid1[0][0], true},
	&check{[]byte("RANT"), testGrid1[3][0], true},
	&check{[]byte("ROCK"), testGrid1[3][0], true},
	&check{[]byte("QADRICENTENNIALS"), testGrid2[0][0], true},
	&check{[]byte("QANT"), testGrid2[0][0], true},
	&check{[]byte("SLEET"), testGrid2[3][0], true},
}

func TestCheckPath(t *testing.T) {
	for _, test := range pathTests {
		visited := make([]*cell, 0)
		result := checkPath(test.word, test.cell, visited)
		if result != test.check {
			t.Errorf("expected %t, but got %t\n", test.check, result)
		}
	}

}
