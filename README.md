# boggle

a terminal-based implementation of the game of Boggle 

---

Borrows the Trie dictionary code from github.com/gammazero/bogglesolver.

# Building

The build expects support for Go modules which is only available in recent versions (1.11 and later) of Go. 

```
$ git clone https://bitbucket.org/_metalogic_/boggle.git
$ export GO111MODULE=on
$ cd boggle/tui
$ go build
```
run it it as 

```
$ ./tui
```

This will create a .boggle file in your home directory to save the running game stats. Works on Mac and no doubt Linux. Not sure about Windows (could be file path issues).

