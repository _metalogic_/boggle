package boggle

import (
	"fmt"
	"strings"

	"bitbucket.org/_metalogic_/log"
)

// Game represents a single game
type Game struct {
	grid grid
	// words found from length 3 to 16 (found[0] to found[13])
	found []string
	// a sorted list of words in the solution from length 3 to 16 (solution[0] to solution[13])
	solution []string
}

// NewGame creates a new game object
func NewGame() (game *Game) {
	game = &Game{}
	game.grid = newGrid()
	game.found = make([]string, 0)
	game.solution = make([]string, 0)
	return game
}

// Reset resets the grid, found and solution for a new game
func (game *Game) Reset() {
	game.grid.roll()
	game.found = make([]string, 0)
	game.solution = make([]string, 0)
}

// Cell returns the grid cell at row i, col j
func (game *Game) Cell(i, j int) string {
	return game.grid.cell(i, j)
}

// Roll resets the game grid with the results of a roll of the dice
func (game *Game) Roll() {
	game.grid.roll()
}

// FoundWords returns the slice of found words
func (game *Game) FoundWords() []string {
	return game.found
}

// SolutionWords returns the slice of words in the solution
func (game *Game) SolutionWords() []string {
	return game.solution
}

// Check returns true for a valid path, and a dictionary word
func (game *Game) Check(dict *Trie, word string) bool {
	log.Debugf("checking '%s'\n", word)

	// handle Qu => Q
	if strings.Contains(word, "Q") && !strings.Contains(word, "QU") {
		return false
	}

	// a single 'Q' means 'Qu' in the grid
	bytes := []byte(strings.ReplaceAll(word, "QU", "Q"))

	for row := 0; row < 4; row++ {
		for col := 0; col < 4; col++ {
			if checkPath(bytes, game.grid[row][col], make([]*cell, 0)) {
				if dict.ContainsWord(word) {
					return true
				}
			}
		}
	}
	return false
}

// Solve finds all solutions for the game, returning true on the done channel when complete
func (game *Game) Solve(dict *Trie, done chan<- bool) {
	for row := 0; row < 4; row++ {
		for col := 0; col < 4; col++ {
			game.findWords(dict, game.grid[row][col], make([]byte, 0), make([]*cell, 0))
		}
	}
	done <- true
}

// findWords ...
func (game *Game) findWords(dict *Trie, c *cell, bytes []byte, visited []*cell) {

	i := len(bytes) - 3

	if i > 13 {
		return
	}

	// a cell may only be used once in a path
	for _, v := range visited {
		if c == v {
			return
		}
	}

	visited = append(visited, c)

	// special treatment for 'Qu'
	if c.letter == 'Q' {
		bytes = append([]byte{'Q', 'U'}, bytes...)
		i = i + 2
	} else {
		bytes = append([]byte{c.letter}, bytes...)
		i = i + 1
	}

	if i >= 0 {
		word := string(bytes)

		log.Trace("checking ", word)
		if dict.ContainsWord(word) {
			game.solution = sortedInsert(word, game.solution)
		}
	}

	for _, neighbor := range c.neighbors {
		game.findWords(dict, neighbor, bytes, visited)
	}
	return
}

// PrintGrid prints a simple text representation of the grid
func (game *Game) PrintGrid() {
	for row := 0; row < 4; row++ {
		fmt.Println("\n  ---   ---   ---   ---")
		fmt.Print("|")
		for col := 0; col < 4; col++ {
			if game.grid[row][col].letter == 'Q' {
				fmt.Printf("  Qu |")
				continue
			}
			fmt.Printf("  %c  |", game.grid[row][col].letter)
		}
	}
	fmt.Println("\n  ---   ---   ---   ---")
}

// PrintAdjacencies ...
func (game *Game) PrintAdjacencies() {
	printAdjacencies(game.grid)
}

// AddFound adds word to the list of games found words
func (game *Game) AddFound(word string) {
	game.found = sortedInsert(word, game.found)
}

// NumFound returns the longest word found if it beats candidate
func (game *Game) NumFound() int {
	return len(game.found)
}

// NumPossible returns the longest word in the solution if it beats candidate
func (game *Game) NumPossible() int {
	return len(game.solution)
}

// MaxFound returns the longest word found if it beats candidate
func (game *Game) MaxFound(current string) string {
	return max(current, game.found)
}

// MaxPossible returns the longest word in the solution if it beats candidate
func (game *Game) MaxPossible(current string) string {
	return max(current, game.solution)
}

func max(current string, words []string) string {
	var candidate string
	for _, w := range words {
		if len(w) > len(candidate) {
			candidate = w
		}
	}
	if len(candidate) > len(current) {
		return candidate
	}
	return current
}
