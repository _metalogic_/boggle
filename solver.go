package boggle

import (
	"fmt"
	"strings"
)

type word struct {
	word   string
	found  bool
	points int
}

func (w word) String() string {
	return fmt.Sprintf("%s found: %t", w.word, w.found)
}

func sortedInsert(word string, words []string) []string {
	n := len(words)
	if n == 0 {
		return append(words, word)
	}

	for i, w := range words {
		n := len(words)
		c := strings.Compare(word, w)
		if c == 0 { // word has already been added
			return words
		}
		if c > 0 {
			if i == n-1 {
				words = append(words, word)
				return words
			}
			continue
		}

		words = append(words, word)

		copy(words[i+1:], words[i:])
		words[i] = word
		return words

	}
	return append([]string{word}, words...)
}

// Found returns true if word has already been found in game
func (game *Game) Found(word string) bool {
	for _, w := range game.found {
		if strings.Compare(word, w) == 0 {
			return true
		}
	}
	return false
}

// Words returns the list of word strings in the solution for words of given length
func (game *Game) Words(length int) (words []string) {
	for _, word := range game.solution {
		if len(word) == length {
			words = append(words, word)
		}
	}
	return words
}

// Points returns the point value of the found words in the game
func (game *Game) Points() int {
	return points(game.found)
}

// SolutionPoints returns the point value of the game solution
func (game *Game) SolutionPoints() int {
	return points(game.solution)
}

// points returns the number of points in a word list
func points(words []string) int {
	var sum = 0
	for _, w := range words {
		switch len(w) {
		case 3, 4:
			sum++
		case 5:
			sum += 2
		case 6:
			sum += 3
		case 7:
			sum += 4
		default:
			sum += 11
		}
	}

	return sum
}
