module bitbucket.org/_metalogic_/color

go 1.12

require (
	bitbucket.org/_metalogic_/colorable v0.0.0-20190331003821-842842382e6d
	bitbucket.org/_metalogic_/isatty v0.0.0-20190331003629-c43982d0478b
)
