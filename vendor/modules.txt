# bitbucket.org/_metalogic_/color v0.0.0-20190331003949-344d6bdb674e
bitbucket.org/_metalogic_/color
# bitbucket.org/_metalogic_/colorable v0.0.0-20190331003821-842842382e6d
bitbucket.org/_metalogic_/colorable
# bitbucket.org/_metalogic_/isatty v0.0.0-20190331003629-c43982d0478b
bitbucket.org/_metalogic_/isatty
# bitbucket.org/_metalogic_/log v1.3.0
bitbucket.org/_metalogic_/log
# github.com/gdamore/encoding v1.0.0
github.com/gdamore/encoding
# github.com/gdamore/tcell v1.1.2
github.com/gdamore/tcell
github.com/gdamore/tcell/terminfo
# github.com/lucasb-eyer/go-colorful v1.0.2
github.com/lucasb-eyer/go-colorful
# github.com/mattn/go-runewidth v0.0.4
github.com/mattn/go-runewidth
# github.com/rivo/tview v0.0.0-20190721135419-23dc8a0944e4
github.com/rivo/tview
# github.com/rivo/uniseg v0.0.0-20190513083848-b9f5b9457d44
github.com/rivo/uniseg
# golang.org/x/sys v0.0.0-20190602015325-4c4f7f33c9ed
golang.org/x/sys/unix
# golang.org/x/text v0.3.0
golang.org/x/text/encoding
golang.org/x/text/transform
golang.org/x/text/encoding/internal/identifier
