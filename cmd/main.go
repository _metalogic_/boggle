package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"

	"bitbucket.org/_metalogic_/boggle"
	"bitbucket.org/_metalogic_/log"
)

const (
	wordsFile = "../dict.txt.gz"
)

var (
	dictionary *boggle.Trie
	count      int
	err        error
	debug      bool
)

func init() {

	debug = (os.Getenv("LOG_LEVEL") == "DEBUG")
	if debug {
		log.SetLevel(log.DebugLevel)
	}

	dictionary, count, err = boggle.LoadWords(wordsFile, 3, 16)
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Debugf("loaded %d words from %s", count, wordsFile)
}

func main() {

	game := boggle.NewGame()

	game.PrintGrid()

	if debug {
		game.PrintAdjacencies()
	}

	// solving takes a few seconds so spawn a goroutine
	done := make(chan bool)
	// solve the game using dictionary
	go game.Solve(dictionary, done)

	reader := bufio.NewReader(os.Stdin)
	var text string
	for start := time.Now(); time.Since(start) < 60*time.Second; {
		fmt.Print("Enter word: ")
		text, _ = reader.ReadString('\n')
		// clean the text for sloppy typers
		text = strings.ToUpper(strings.ReplaceAll(strings.Trim(text, "\t\n"), " ", ""))

		if len(text) < 3 || len(text) > 16 {
			fmt.Println("words must be between 3 and 16 letters long")
			continue
		}

		if game.Found(text) || !game.Check(dictionary, text) {
			continue
		}

		fmt.Println("found " + strings.ToLower(text))

	}

	// print the solution when it's done
	printSolution(game, done)

}

func printSolution(game *boggle.Game, done <-chan bool) {
	// wait for the solver to complete
	<-done
	fmt.Print("\n Solution \n")
	fmt.Printf("scored %d points of %d available\n", game.Points(), game.SolutionPoints())
	for i := 3; i < 16; i++ {
		words := game.Words(i)
		n := len(words)
		if n > 0 {
			fmt.Printf("\n%d words of length %d:\n", n, i)
			fmt.Println("--------------------")
			for _, w := range words {
				fmt.Print(strings.ToLower(w), ", ")
			}
		}
	}
}
