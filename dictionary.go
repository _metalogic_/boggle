package boggle

import (
	"bufio"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"sort"
	"strings"
)

var (
	trie  *Trie
	count int
)

// LoadWords reads a file of words and creates a trie containing them.
func LoadWords(wordsFile string, minLen, maxLen int) (trie *Trie, count int, err error) {
	f, err := os.Open(wordsFile)
	if err != nil {
		return trie, count, fmt.Errorf("solver: error opening words file: %s", err)
	}
	defer f.Close()

	var rdr io.Reader
	if strings.HasSuffix(wordsFile, ".gz") {
		rdr, err = gzip.NewReader(f)
		if err != nil {
			return trie, count, fmt.Errorf("solver: error unzipping words file: %s", err)
		}
	} else {
		rdr = f
	}
	scanner := bufio.NewScanner(rdr)
	trie = NewTrie()
	count = 0
	var word string

	// Scan through line-dilimited word list.
	for scanner.Scan() {
		word = scanner.Text()
		// Skip words that are too long or too short.
		if len(word) > maxLen || len(word) < minLen {
			continue
		}
		// Skip words that start with a capital letter.
		if int(word[0]) < 'a' {
			continue
		}
		trie.Insert(word)
		count++
	}

	if err := scanner.Err(); err != nil {
		return trie, count, fmt.Errorf("solver: error reading words file: %s", err)
	}

	return trie, count, nil
}

func uniqueSortedWords(words []string) []string {
	if len(words) == 0 {
		return words
	}
	sort.Sort(sort.StringSlice(words))
	unique := make([]string, 0, len(words))
	var prev string
	for _, w := range words {
		if w != prev {
			unique = append(unique, w)
			prev = w
		}
	}
	return unique
}
