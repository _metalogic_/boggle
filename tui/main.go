// Demo code for the Flex primitive.
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"sync"
	"time"

	"bitbucket.org/_metalogic_/boggle"
	"bitbucket.org/_metalogic_/log"
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

const (
	dictFile             = "../dict.txt.gz"
	backgroundColor      = tcell.ColorBlack
	textColor            = tcell.ColorWhite
	inputBackgroundColor = tcell.ColorLightGray
	inputTextColor       = tcell.ColorBlack
)

var (
	dictionary *boggle.Trie
	app        *tview.Application
	table      = tview.NewTable().SetBorders(true)
	padding    = tview.NewTextView().SetText(" ")
	grid       = tview.NewFlex().AddItem(padding, 0, 1, false).AddItem(table, 0, 20, false)
	word       = tview.NewInputField().
			SetLabel(" Word: ").
			SetFieldWidth(16).
			SetAcceptanceFunc(boggleWord).
			SetLabelColor(textColor).
			SetFieldBackgroundColor(inputBackgroundColor).
			SetFieldTextColor(inputTextColor)
	found    = tview.NewTextView().SetTextColor(textColor)
	left     = tview.NewFlex().SetDirection(tview.FlexRow)
	right    = tview.NewFlex().SetDirection(tview.FlexRow)
	stats    = tview.NewTextView().SetWordWrap(true) // .SetBorder(true).SetTitle("Stats")
	results  = tview.NewTextView().SetWordWrap(true)
	info     = tview.NewTextView()
	game     *boggle.Game
	gend     time.Time
	playtime = 180 * time.Second
	newGame  = make(chan bool)

	gstats    *GameStats
	statsFile string
)

func init() {

	debug := (os.Getenv("LOG_LEVEL") == "DEBUG")
	if debug {
		log.SetLevel(log.DebugLevel)
	}
	app = tview.NewApplication()
	statsFile = os.Getenv("HOME") + "/.boggle"
	gstats = mustReadStats(statsFile)
	stats.SetText(statsText(gstats))

	var count int
	var err error
	if dictionary, count, err = boggle.LoadWords(dictFile, 3, 16); err != nil {
		log.Fatal(err.Error())
	}
	log.Debugf("loaded %d words from %s", count, dictFile)
}

// GameStats holds the running tally of a user's game statistics for all plays
type GameStats struct {
	NumGames    int    `json:"numGames"`
	NumFound    int    `json:"numFound"`
	NumPossible int    `json:"numPossible"`
	MaxWord     string `json:"maxWord"`
	MaxPossible string `json:"maxPossible"`
}

// update stats with results of game
func (stats *GameStats) updateStats(game *boggle.Game) {
	stats.NumGames = stats.NumGames + 1
	stats.NumFound = stats.NumFound + game.NumFound()
	stats.NumPossible = stats.NumPossible + game.NumPossible()
	stats.MaxWord = game.MaxFound(stats.MaxWord)
	stats.MaxPossible = game.MaxPossible(stats.MaxPossible)
}

// save stats to .boogle
func (stats *GameStats) saveStats(file string) error {
	// marshall null GameStats
	data, err := json.Marshal(stats)
	if err != nil {
		return err
	}
	// write initial GameStats to stats file
	err = ioutil.WriteFile(file, data, os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}

func main() {

	left.AddItem(clock, 1, 1, false)
	left.AddItem(grid, 10, 10, false)
	left.AddItem(word, 1, 1, true)
	left.AddItem(found, 0, 15, false)

	right.AddItem(stats, 3, 3, false)
	right.AddItem(results, 0, 6, false)

	word.SetDoneFunc(enter)

	flex := tview.NewFlex().
		AddItem(left, 30, 1, true).
		AddItem(right, 0, 1, false)

	view := tview.NewFlex().SetDirection(tview.FlexRow).AddItem(flex, 0, 50, true).AddItem(info, 0, 1, false)

	// play and timer goroutines coordinate on endGame condition
	lock := &sync.Mutex{}
	lock.Lock()
	endGame := sync.NewCond(lock)

	go play(time.Now(), playtime, endGame, newGame)
	newGame <- true

	go timer(endGame)

	if err := app.SetRoot(view, true).Run(); err != nil {
		panic(err)
	}
}

func boggleWord(text string, ch rune) bool {
	// any upper case letter quits the game
	for _, c := range text {
		if c >= 65 && c <= 90 {
			app.Stop()
			return false
		}
	}
	// at end of game space bar starts a new game
	if gameOver() {
		for _, c := range text {
			if c == ' ' {
				newGame <- true
				return false
			}
		}
		return false
	}

	// words are entered as ASCII lowercase letters
	for _, c := range text {
		if !(c >= 97 && c <= 122) {
			return false
		}
	}
	// words must be 16 letters or less
	return len([]rune(text)) <= 16
}

// enter is called when the the player hits enter to submit her word
func enter(key tcell.Key) {
	text := word.GetText()
	// clean the text for sloppy typers; convert to upper case for dict and grid comparisons
	text = strings.ToUpper(strings.ReplaceAll(strings.Trim(text, "\t\n"), " ", ""))

	if len(text) < 3 || len(text) > 16 {
		word.SetText("")
		return
	}
	if key == tcell.KeyEnter {
		if gameOver() {
			word.SetText("")
			return
		}
		if len(text) >= 3 {
			word.SetText("")
			addWord(text)
		}
	}
}

func addWord(word string) {
	if gameOver() {
		return
	}
	// if word already found or not in dictionary don't add it
	if game.Found(word) || !game.Check(dictionary, word) {
		return
	}

	game.AddFound(word)

	var list string
	for _, w := range game.FoundWords() {
		w = strings.ToLower(w)
		list = fmt.Sprintf(" %s\n%s", w, list)
	}
	found.SetText("\n" + list)
}

func gameOver() bool {
	return remaining <= 0
}

func play(start time.Time, duration time.Duration, endGame *sync.Cond, another chan bool) {
	info.SetText(" Key space bar to start another game or an upper case letter to quit")

	// game is a singleton holding the current game in play
	game = boggle.NewGame()

	for {
		// solving takes a few seconds so spawn a goroutine
		done := make(chan bool)
		go game.Solve(dictionary, done)

		// wait on channel to start game
		<-another
		results.Clear()
		start = time.Now()

		// end the game duration from start
		gend = start.Add(duration)

		drawGrid(game)

		// wait for end of game
		endGame.Wait()

		// wipe found words
		found.SetText("")

		// update stats with game results
		gstats.updateStats(game)

		// save and display current game stats
		gstats.saveStats(statsFile)
		stats.SetText(statsText(gstats))
		results.SetText(resultsText(game))
		app.Draw()

		// get another game ready
		game.Reset()
	}

}

func drawGrid(game *boggle.Game) {
	for r := 0; r < 4; r++ {
		for c := 0; c < 4; c++ {
			table.SetCell(r, c,
				tview.NewTableCell(game.Cell(r, c)).
					SetTextColor(tcell.ColorWhite).
					SetAlign(tview.AlignCenter))
		}
	}
}

func statsText(stats *GameStats) string {
	if stats.NumGames == 0 {
		return "Percentage: 0% over 0 games"
	}
	return fmt.Sprintf("Running Percentage: %.2f%% over %d games\nLongest found: %s (%d)\nLongest Possible: %s (%d)",
		100*float64(stats.NumFound)/float64(stats.NumPossible),
		stats.NumGames,
		stats.MaxWord, len(stats.MaxWord),
		stats.MaxPossible, len(stats.MaxPossible))
}

func mustReadStats(file string) (stats *GameStats) {
	stats = &GameStats{}
	data, err := ioutil.ReadFile(file)
	if err != nil {
		err = stats.saveStats(file)
		if err != nil {
			log.Fatal(err.Error())
		}
		return stats
	}
	err = json.Unmarshal(data, stats)
	if err != nil {
		log.Fatal(err.Error())
	}
	return stats
}

func resultsText(game *boggle.Game) string {
	return fmt.Sprintf("-----------\nPercentage: %.2f%%\n\nFound [%d]:\n%s\n\nPossible [%d]:\n%s\n",
		100*float64(game.NumFound())/float64(game.NumPossible()),
		game.NumFound(), game.FoundWords(),
		game.NumPossible(), game.SolutionWords())
}
