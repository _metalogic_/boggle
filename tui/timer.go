package main

import (
	"fmt"
	"sync"
	"time"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

const (
	refreshInterval = 500 * time.Millisecond
)

var (
	clock     *tview.Box
	remaining time.Duration
)

func init() {
	clock = tview.NewBox()
	clock.SetDrawFunc(drawTimer)
}

func drawTimer(screen tcell.Screen, x int, y int, width int, height int) (int, int, int, int) {
	var timeStr string
	if gameOver() {
		timeStr = "\n Time: 0:00"
	} else {
		min := int(remaining.Minutes())
		sec := int(remaining.Seconds()) - 60*min
		timeStr = fmt.Sprintf("\n Time: %d:%02d", min, sec)
	}
	tview.Print(screen, timeStr, 0, 0, 20, tview.AlignLeft, tcell.ColorLime)
	return 0, 0, 0, 0
}

func timer(endGame *sync.Cond) {
	tick := time.NewTicker(refreshInterval)
	for {
		select {
		case <-tick.C:
			remaining = time.Until(gend)
			if gameOver() {
				endGame.Broadcast()
			}
			app.Draw()
		}
	}
}
