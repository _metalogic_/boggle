package boggle

import (
	"math/rand"
	"time"
)

// the 16 six-faced dice for the original Boggle as reported
// by Phil Fleischmann on rpggeek.com
// 'Q' becomes 'Qu' when rendered on the board
var (
	dice = [][]byte{
		{'A', 'A', 'C', 'I', 'O', 'T'},
		{'A', 'B', 'I', 'L', 'T', 'Y'},
		{'A', 'B', 'J', 'M', 'O', 'Q'},
		{'A', 'C', 'D', 'E', 'M', 'P'},
		{'A', 'C', 'E', 'L', 'R', 'S'},
		{'A', 'D', 'E', 'N', 'V', 'Z'},
		{'A', 'H', 'M', 'O', 'R', 'S'},
		{'B', 'F', 'I', 'O', 'R', 'X'},
		{'D', 'E', 'N', 'O', 'S', 'W'},
		{'D', 'K', 'N', 'O', 'T', 'U'},
		{'E', 'E', 'F', 'H', 'I', 'Y'},
		{'E', 'G', 'I', 'N', 'T', 'V'},
		{'E', 'G', 'K', 'L', 'U', 'Y'},
		{'E', 'H', 'I', 'N', 'P', 'S'},
		{'E', 'L', 'P', 'S', 'T', 'U'},
		{'G', 'I', 'L', 'R', 'U', 'W'},
	}
)

// roll a Boggle die to generate a random letter for the grid
func roll(i int) byte {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	return dice[i][r1.Intn(6)]
}
